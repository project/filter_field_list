/**
 * @file
 * README file for Filter Field List.
 */

FILTER FIELD LIST Module

CONTENTS
--------

1.  Introduction
2.  Configuration

1.  Introduction
----------------

FILTER FIELD LIST

1. Introduction
-------------
This module provides Advanced field filtering functionality on /admin/reports/fields page,
where user get Flexibility in term of used data available in entire site along with Used in bundle & (Data count).

2.  Configuration
-----------------
*  Download and install the module.
*  Go to 'admin/reports/fields' and you will get Advanced Filter on the top of the table. there you can SET the
   filter value for field type and field and pull the specific data Field name/ Field type/ Used in bundle (Data count).

